package net.mesvaccins.api.client;

import net.mesvaccins.api.client.models.HealthProfessional;

public class FixtureHealthProfessional implements HealthProfessional {

	private final int organisationId;

	FixtureHealthProfessional(MVXConfiguration configuration) {
		organisationId = configuration.getOrganisationId();
	}

	public String getLogin() {
		return "pierre.borsan@gmail.com";
	}

	public int getOrganisationId() {
		return organisationId;
	}
}
