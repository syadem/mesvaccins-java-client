package net.mesvaccins.api.client;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import net.mesvaccins.api.client.models.Certificate;
import net.mesvaccins.api.client.models.ImmunizationRecord;
import net.mesvaccins.api.client.requests.GetCertificateRequest;
import net.mesvaccins.api.client.requests.GetVaccinationRecordsRequest;

import java.io.Console;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Unit test for MVX API.
 */
public class AppTest extends TestCase {
  private static final ServerContext.Environment ENVIRONMENT = ServerContext.Environment.Integration;
  private MVXConfiguration configuration;

  public AppTest(String testName) throws IOException {
    super(testName);
    configuration = new MVXConfiguration();
  }

  /**
   * @return the suite of tests being tested
   */
  public static Test suite() {
    return new TestSuite(AppTest.class);
  }

  public void testGetCertificate() throws Exception {
    Path certificatePath = Paths.get(getCertificateFilename());
    if (!Files.exists(certificatePath)) {
      CertificateManagementContext context = new CertificateManagementContext(ENVIRONMENT);
      Certificate certificate = new GetCertificateRequest(configuration.getOTP()).send(context);

      assert(certificate.isOk());
      assertEquals("OK", certificate.getStatusDescription());
      assertNotNull(certificate.getCertificateContent());
      Files.write(certificatePath, certificate.getCertificateContent());
    }
  }

  public void testGetRecords() throws Exception {
    MVXContext context = new MVXContext(configuration.getApplicationToken(), configuration.getApplicationSecret(), ENVIRONMENT, getCertificateFilename(), configuration.getOTP());
    context.setCurrentHealthProfessional(new FixtureHealthProfessional(configuration));

    List<ImmunizationRecord> records = new GetVaccinationRecordsRequest().send(context);

    assertNotNull(records);
    assertFalse(records.isEmpty());
  }

  private String getCertificateFilename() {
    return "certificate_" + ENVIRONMENT.toString().toLowerCase() + ".p12";
  }
}
