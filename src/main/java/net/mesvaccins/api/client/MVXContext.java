package net.mesvaccins.api.client;

import net.mesvaccins.api.client.models.HealthProfessional;
import net.mesvaccins.api.client.models.SessionIsLogged;
import net.mesvaccins.api.client.models.SessionLogin;
import net.mesvaccins.api.client.requests.SessionIsLoggedRequest;
import net.mesvaccins.api.client.requests.SessionLoginRequest;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;

public class MVXContext extends SecuredServerContext {

  private String applicationToken;
  private String applicationSecret;

  private HealthProfessional currentHealthProfessional;
  private String currentSession;

  MVXContext(String applicationToken, String applicationSecret, Environment environment, String certificateFilename, String certificatePassword) {
    super(certificateFilename, certificatePassword, environment);
    this.applicationToken = applicationToken;
    this.applicationSecret = applicationSecret;
  }

  @Override
  protected ClientBuilder configureClient(ClientBuilder clientBuilder) throws Exception {
    HttpAuthenticationFeature basicAuthentication = HttpAuthenticationFeature.basic(applicationToken, applicationSecret);
    return super.configureClient(clientBuilder).register(basicAuthentication);
  }

  @Override
  protected void prepareInvocation(Invocation.Builder invocationBuilder) throws Exception {
    refreshSession();
    invocationBuilder.cookie("sessionids", currentSession);
  }

  private void refreshSession() throws Exception {
    SessionIsLoggedRequest heartbeat = new SessionIsLoggedRequest(currentSession);
    SessionIsLogged response = heartbeat.send(createIDSContext());
    if (!response.isLogged()) {
      setCurrentHealthProfessional(currentHealthProfessional);
    }
  }

  public void setCurrentHealthProfessional(HealthProfessional healthProfessional) throws Exception {
    SessionLoginRequest loginRequest = new SessionLoginRequest(healthProfessional.getOrganisationId(), applicationToken, healthProfessional.getLogin());
    SessionLogin response = loginRequest.send(createIDSContext());
    if (response.getStatus() == 0) {
      currentSession = response.getSessionId();
      currentHealthProfessional = healthProfessional;
    } else {
      throw new Exception("IDS error: " + response.getStatusDescription());
    }
  }

  @Override
  protected String getApplicationUri() {
    switch (getEnvironment()) {
      case Integration:
        return "https://test-pro-secure.mesvaccins.net/mvx_api/v1";
      case Production:
        return "https://pro-secure.mesvaccins.net/mvx_api/v1";
      default:
        return null;
    }
  }

  private IDSContext createIDSContext() {
    return new IDSContext(getCertificateFilename(), getCertificatePassword(), getEnvironment());
  }
}
