package net.mesvaccins.api.client;

import org.glassfish.jersey.jackson.JacksonFeature;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.function.Function;

public abstract class ServerContext {
  public enum Environment {
    Integration,
    Production
  }

  private Environment environment;

  ServerContext(Environment environment) {
    this.environment = environment;
  }

  Environment getEnvironment() {
    return environment;
  }

  protected abstract String getApplicationUri();

  public WebTarget getTarget() throws Exception {

    Client client = configureClient(ClientBuilder.newBuilder()
        .register(JacksonFeature.class))
        .build();
    return client.target(getApplicationUri());
  }

  protected ClientBuilder configureClient(ClientBuilder clientBuilder) throws Exception {
    return clientBuilder;
  }

  public Response getResponse(WebTarget target, Function<Invocation.Builder, Response> invocator) throws Exception {
    Invocation.Builder invocationBuilder = target.request(MediaType.APPLICATION_JSON);
    prepareInvocation(invocationBuilder);

    Response response = invocator.apply(invocationBuilder);
    if (response.getStatus() != 200) {
      throw new Exception("Http error code " + response.getStatus() + ": " + response.getStatusInfo().getReasonPhrase());
    }
    return response;
  }

  protected void prepareInvocation(Invocation.Builder invocationBuilder) throws Exception {
  }
}
