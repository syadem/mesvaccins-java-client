package net.mesvaccins.api.client;

import org.glassfish.jersey.SslConfigurator;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.ws.rs.client.ClientBuilder;
import java.io.FileInputStream;
import java.security.KeyStore;

public abstract class SecuredServerContext extends ServerContext {

  private String certificateFilename;
  private String certificatePassword;

  SecuredServerContext(String certificateFilename, String certificatePassword, Environment environment) {
    super(environment);
    this.certificateFilename = certificateFilename;
    this.certificatePassword = certificatePassword;
  }

  String getCertificateFilename() { return certificateFilename; }
  String getCertificatePassword() { return certificatePassword; }

  @Override
  protected ClientBuilder configureClient(ClientBuilder clientBuilder) throws Exception {
    KeyStore keyStore = KeyStore.getInstance("pkcs12");
    keyStore.load(new FileInputStream(certificateFilename), certificatePassword.toCharArray());
    SslConfigurator sslConfig = SslConfigurator.newInstance()
        .trustStorePassword(certificatePassword)
        .trustStoreType("JKS")
        .keyStore(keyStore)
        .keyManagerFactoryAlgorithm("SunX509")
        .keyPassword(certificatePassword);
    SSLContext sslContext = sslConfig.createSSLContext();
    return super.configureClient(clientBuilder).sslContext(sslContext);
  }
}
