package net.mesvaccins.api.client;

import javax.ws.rs.client.Invocation;

public class IDSContext extends SecuredServerContext {

  IDSContext(String certificateFilename, String certificatePassword, Environment environment) {
    super(certificateFilename, certificatePassword, environment);
  }

  @Override
  protected void prepareInvocation(Invocation.Builder invocationBuilder) {
  }

  @Override
  protected String getApplicationUri() {
    switch (getEnvironment()) {
      case Integration:
        return "https://test-pro-secure.mesvaccins.net/authenticationids";
      case Production:
        return "https://pro-secure.mesvaccins.net/authenticationids";
    }
    return null;
  }
}
