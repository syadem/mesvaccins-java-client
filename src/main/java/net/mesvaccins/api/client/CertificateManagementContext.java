package net.mesvaccins.api.client;

public class CertificateManagementContext extends ServerContext {
  CertificateManagementContext(Environment environment) {
    super(environment);
  }

  @Override
  protected String getApplicationUri() {
    switch (getEnvironment()) {
      case Integration:
        return "https://certtest.idshost.fr";
      case Production:
        return "https://cert.idshost.fr";
    }
    return null;
  }
}
