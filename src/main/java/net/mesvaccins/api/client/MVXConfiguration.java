package net.mesvaccins.api.client;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

class MVXConfiguration {
  private Properties properties;

  String getApplicationToken() {
    return properties.get("application_token").toString();
  }

  String getApplicationSecret() {
    return properties.get("application_secret").toString();
  }

  int getOrganisationId() {
    return Integer.parseInt(properties.get("organisation_id").toString());
  }

  String getOTP() {
    return properties.get("OTP").toString();
  }

  MVXConfiguration() throws IOException {
    File file = new File("mvx.properties");
    FileInputStream fileInput = new FileInputStream(file);
    properties = new Properties();
    properties.load(fileInput);
  }
}
