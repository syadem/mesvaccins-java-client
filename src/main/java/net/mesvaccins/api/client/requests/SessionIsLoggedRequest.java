package net.mesvaccins.api.client.requests;

import net.mesvaccins.api.client.ServerContext;
import net.mesvaccins.api.client.models.SessionIsLogged;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.Hashtable;
import java.util.Map;

public class SessionIsLoggedRequest implements Request<SessionIsLogged> {
  private String session;

  public SessionIsLoggedRequest(String session) {
    this.session = session;
  }

  public SessionIsLogged send(ServerContext context) throws Exception {
    Map<String, String> params = new Hashtable<>();
    params.put("sessionids", session);
    WebTarget target = context.getTarget().path("restisloggedservice.php");
    return context.getResponse(target, (invocation) -> invocation.post(Entity.entity(params, MediaType.APPLICATION_JSON_TYPE))).readEntity(SessionIsLogged.class);
  }
}
