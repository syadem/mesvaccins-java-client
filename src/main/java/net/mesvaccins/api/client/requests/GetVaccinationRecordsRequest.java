package net.mesvaccins.api.client.requests;

import net.mesvaccins.api.client.ServerContext;
import net.mesvaccins.api.client.models.ImmunizationRecord;

import javax.ws.rs.client.SyncInvoker;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import java.util.List;

public class GetVaccinationRecordsRequest implements Request<List<ImmunizationRecord>> {
  public List<ImmunizationRecord> send(ServerContext context) throws Exception {
    WebTarget target = context.getTarget().path("records.json");

    return context.getResponse(target, SyncInvoker::get).readEntity(new GenericType<List<ImmunizationRecord>>() {
    });
  }
}
