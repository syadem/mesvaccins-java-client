package net.mesvaccins.api.client.requests;

import net.mesvaccins.api.client.ServerContext;

public interface Request<T> {
  T send(ServerContext context) throws Exception;
}
