package net.mesvaccins.api.client.requests;

import net.mesvaccins.api.client.ServerContext;
import net.mesvaccins.api.client.models.Certificate;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.Hashtable;
import java.util.Map;

public class GetCertificateRequest implements Request<Certificate> {

  private String otp;

  public GetCertificateRequest(String otp) {
    this.otp = otp;
  }

  @Override
  public Certificate send(ServerContext context) throws Exception {
    Map<String, String> params = new Hashtable<>();
    params.put("identifier", "none");
    params.put("otp", otp);
    params.put("type", "p12");
    WebTarget target = context.getTarget().path("restgetcertificate.php");
    return context.getResponse(target, (invocation) -> invocation.post(Entity.entity(params, MediaType.APPLICATION_JSON_TYPE))).readEntity(Certificate.class);
  }
}
