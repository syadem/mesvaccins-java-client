package net.mesvaccins.api.client.requests;

import net.mesvaccins.api.client.ServerContext;
import net.mesvaccins.api.client.models.SessionLogin;
import org.mindrot.jbcrypt.BCrypt;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.Hashtable;
import java.util.Map;

public class SessionLoginRequest implements Request<SessionLogin> {
  private String applicationToken;
  private int organisationId;
  private String login;

  public SessionLoginRequest(int organisationId, String applicationToken, String login) {
    this.organisationId = organisationId;
    this.applicationToken = applicationToken;
    this.login = login;
  }

  public SessionLogin send(ServerContext context) throws Exception {
    Map<String, String> params = new Hashtable<>();
    params.put("authentifier", login);
    params.put("password", computePassword(login, organisationId, applicationToken));
    WebTarget target = context.getTarget().path("restloginservice.php");
    return context.getResponse(target, (invocation) -> invocation.post(Entity.entity(params, MediaType.APPLICATION_JSON_TYPE))).readEntity(SessionLogin.class);
  }

  private static String computePassword(String login, int organisationId, String applicationToken) {
    return BCrypt.hashpw(login + String.valueOf(organisationId), "$2a$10$" + applicationToken);
  }
}
