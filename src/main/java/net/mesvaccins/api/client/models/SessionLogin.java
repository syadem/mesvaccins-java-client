package net.mesvaccins.api.client.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionLogin {
  @JsonProperty("status")
  private int status;
  @JsonProperty("sessionids")
  private String sessionId;

  public int getStatus() {
    return status;
  }

  public String getSessionId() {
    return sessionId;
  }

  public String getStatusDescription() {
    switch (status) {
      case 0:
        return "OK";
      case 1:
        return "Requête mal formée";
      case 2:
        return "Certificat non accepté";
      case 3:
        return "Identifiant manquant (mot de passe et/ou authentifiant)";
      case 4:
        return "Certificat en blacklist";
      case 5:
        return "Identifiants invalides";
    }
    return "";
  }
}
