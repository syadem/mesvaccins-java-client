package net.mesvaccins.api.client.models;

public interface HealthProfessional {
  String getLogin();
  int getOrganisationId();
}
