package net.mesvaccins.api.client.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ImmunizationRecord {
  @JsonProperty("uuid")
  private String uuid;
  @JsonProperty("birth_date")
  private Date birthDate;
  @JsonProperty("gender")
  private String gender;
  @JsonProperty("first_name")
  private String firstName;
  @JsonProperty("last_name")
  private String lastName;
  @JsonProperty("birth_name")
  private String birthName;
  @JsonProperty("place_of_birth")
  private String placeOfBirth;
  @JsonProperty("department_of_birth")
  private int departmentOfBirth;
  @JsonProperty("zip_code")
  private String zipCode;
  @JsonProperty("email")
  private String email;
  @JsonProperty("ins_c")
  private String insC;
  @JsonProperty("home_phone")
  private String homePhone;
  @JsonProperty("mobile_phone")
  private String mobilePhone;
  @JsonProperty("sharing_status")
  private String sharingStatus;
  @JsonProperty("updated_at")
  private Date updatedAt;
  @JsonProperty("created_at")
  private Date createdAt;
}
