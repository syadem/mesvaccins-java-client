package net.mesvaccins.api.client.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SessionIsLogged {
  @JsonProperty("status")
  private int status;

  public boolean isLogged() {
    return status == 0;
  }
}
