package net.mesvaccins.api.client.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.xml.bind.DatatypeConverter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Certificate {
  @JsonProperty("status")
  private int status;
  @JsonProperty("certbase64encoded")
  private String encodedCertificate;

  private int getStatus() {
    return status;
  }

  public byte[] getCertificateContent() {
    return DatatypeConverter.parseBase64Binary(encodedCertificate);
  }

  public boolean isOk() {
    return getStatus() == 0;
  }

  public String getStatusDescription() {
    switch (status) {
      case 0:
        return "OK";
      case 1:
        return "Requête mal formée";
      case 2:
        return "Type incorrect (pem/p12)";
      case 3:
        return "Identifiant ou OTP incorrect";
      case 4:
        return "Erreur serveur, contactez nous à developers@mesvaccins.net";
    }
    return "";
  }
}
