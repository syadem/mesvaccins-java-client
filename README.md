# Client MesVaccins professionnel en Java

Exemple d'implémentation de l'API professionnelle [MesVaccins.net](http://www.mesvaccins.net).

Cette implémentation est fournie à titre d'exemple uniquement.

## Configuration de l'application

Pour faire fonctionner ce projet, il est au préalable nécessaire de renseigner les constantes *ApplicationToken* et *ApplicationSecret* spécifiques à votre application, l'OTP permettant le retrait du certificat et l'identifiant numérique de l'organisation utilisée pour les tests dans un fichier *mvx.properties* placé à la racine du dépôt : 

	application_token=...
	application_secret=...
	organisation_id=...
	OTP=...

Ces éléments sont fournis par le support MesVaccins.net.

## Dépendances

Ce projet utilise la bibliothéque [Jersey](https://jersey.java.net) pour l'accès aux webservices REST et [Jackson](http://jackson.codehaus.org) pour la désérialisation des réponses JSON.

## License

	The MIT License (MIT)
	
	Copyright (c) 2015 Josselin Auguste / Syadem
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.

## Documentation

La documentation complète des APIs MesVaccins.net est accessible en ligne : [https://www.mesvaccins.net/documentation](https://www.mesvaccins.net/documentation).

## Nous contacter

Vous pouvez nous contacter à [developers@mesvaccins.net](mailto:developers@mesvaccins.net).
